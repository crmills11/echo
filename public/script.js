$.material.init();

var json = {
    title: "Receiving Feedback",
    showProgressBar: "top",
    pages: [{
        questions: [{
            type: "matrix",
            name: "header",
            title: "Please select 1 for 'rarely', 2 for 'sometimes' and 3 for 'often'to indicate how consistently you use the described behavior in the workplace.",
            columns: [{
                value: 1,
                text: "1"
            }, {
                value: 2,
                text: "2"
            }, {
                value: 3,
                text: "3"
            },],
            rows: [{
                value: "truly",
                text: "I truly listen to what feedback givers are saying."
            }, {
                value: "overeact",
                text: "I keep feedback in perspective and don’t overreact."
            },{
                value: "learn",
                text: "I try to learn from all feedback, even if it is poorly given."
            },{
                value: "admit",
                text: "I am willing to admit to and learn from questions about my performance or behavior at work."
           }, {
                value: "turn",
                text: "Rather than avoiding feedback, I attempt to turn every feedback session into a useful encounter."
            }, {
                value: "redirection",
                text: "I accept redirection and reinforcement rather than denying them."
            }, {
                value: "team",
                text: "I accept responsibility for my role in achieving individual, team, and organizational goals."
            }, {
                value: "solutions",
                text: "I accept responsibility for searching for solutions to performance and behavioral problems that threaten goals. provide input as needed in developing an action plan for meeting behavioral or performance goals."
            }, {
                value: "emotions",
                text: "I accept responsibility for keeping my emotions in check during feedback discussions."
            },{
                value: "committed",
                text: "I am committed to listening and learning in all feedback situations."
            }]
        }]
        }]
   }     
Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrapmaterial";

var survey = new Survey.Model(json);

survey.onComplete.add(function(result) {

	var sum = 0;
	var rows = Object.values(result.data);
	for(var i = 0; i < rows.length; i++){
		var row = Object.values( rows[i] );
		for(var j = 0; j < row.length; j++) {
			var val = row[j];
			sum += parseInt(val);
		}
	}


function myFunction(sum) 

{
    var scoring;
    if (sum <= 30 && sum >= 24) {
        scoring = "You’re a feedback fiend! You likely receive feedback well, and sort out the constructive criticism from the non-constructive with ease. Think of how you can use that ability to help ot";
    } else if (sum <= 23 && sum >= 16) {
        scoring = "You can receive feedback some of the time. Are there certain environments or situations that hurt your ability to receive feedback?";
    } else if (sum <= 15 && sum >= 10) {
        scoring = "Don’t take this the wrong way… but you may not be very good at receiving feedback. Try to take emotion out of a feedback situation, and listen only for what the critic is saying in concrete terms. Are there specific criticisms that really get to you? Try to think about what makes you turn off, and stop listening.";
    } else {
        scoring = "Error";
    }
    
document.querySelector('#result').innerHTML = "Your Score " + sum + "<br>" + "According to the survey, " + scoring;

} 

myFunction(sum);

});
       
survey.render("surveyElement");